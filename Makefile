all: sampleC

sampleC: sampleC.o strdeque.o strdequeconst.o
	g++ -o sampleC sampleC.o strdeque.o strdequeconst.o
	strip sampleC
	
sampleCPP: test1.o strdeque.o strdequeconst.o
	g++ -o sampleCPP test1.o strdeque.o strdequeconst.o
	strip sampleCPP
	
sample.dbg: sampleC.o strdeque.o strdequeconst.o
	g++ -o sample sampleC.o strdeque.o strdequeconst.o

debug: sample.dbg


sampleC.o: sampleC.c
	gcc -g -c sampleC.c

	
test1.o: test1.cc
	g++ -std=c++14 -g -c test1.cc

	
strdeque.o: strdeque.cc
	g++ -std=c++11 -g -c strdeque.cc

strdequeconst.o: strdequeconst.cc
	g++ -std=c++11 -g -c strdequeconst.cc




clean: 
	rm -f *.o sampleC sampleCPP

.PHONY: all debug clean
	