#ifndef STRDEQUECONST_H
#define STRDEQUECONST_H
	#ifdef __cplusplus
		extern "C" {
	#else
		#include <stdlib.h>
	#endif
    
			unsigned long emptystrdeque();
    
	#ifdef __cplusplus
		}
	#endif
#endif
