#include<iostream>
#include<deque>
#include<string>
#include<cassert>
#include<unordered_map>


namespace {
    
	#ifndef NDEBUG
	const bool debug = true;
	#else
	const bool debug = false;
	#endif
    
	using dequeMap = std::unordered_map<unsigned long, std::deque<std::string>>;
    
	unsigned long& constDequeId() {
		static unsigned long* constDequeId =  new unsigned long(0);
		return *constDequeId;
	}
	
	unsigned long& nextId() {
		static unsigned long* nextId =  new unsigned long(1);
		return *nextId;
	}
    
	dequeMap& queues() {
		static dequeMap* queues = new dequeMap(); 
		return *queues;
	}
	
	/* function checking whether deque id exist or not */
	bool strdeque_exist(unsigned long id) {
		return queues().count(id) > 0;
	}
    
	/* function checking whether deque id empty or not */
	bool strdeque_empty(unsigned long id) {
		if(strdeque_exist(id)) return queues()[id].size() == 0;
		return false;
	}
    
	void const_strqueue(const std::string &message) {
		if (debug) {
			static std::ios_base::Init initializer;
			std::cerr << message << std::endl;
		}
	}
	
	void print_debug(const std::string &message) {
		if (debug) {
			static std::ios_base::Init initializer;
			std::cerr << message << std::endl;
		}
	}
    
}

namespace jnp1 {
    
	extern "C" {
		unsigned long strdeque_new() {
			std::deque<std::string> q;
			unsigned long id = nextId();
			queues()[id] = q;
			nextId()++;
            
			/* printing  debugs*/
			print_debug(std::string(__func__) + ": deque " + std::to_string(id) + " is created");
        
			return id;
		}
    
		void strdeque_delete(unsigned long id) {
			if(id == constDequeId()) {
				const_strqueue("delete");
				return;
			}
			
			if(!strdeque_exist(id)){
				print_debug("strdeque_delete: deque " + std::to_string(id) + " does not exist");
				return;
			}

			queues().erase(id);
			print_debug("strdeque_delete: deque " + std::to_string(id) + " is deleted");
		}
    
		size_t strdeque_size(unsigned long id) {
			size_t queueSize = 0;
            
			if(id == 0) {
				return 0;
			}
            
			if(!strdeque_exist(id)) {
				print_debug("strdeque_size: deque " + std::to_string(id) + " does not exist");
				queueSize = 0;;
			}
			else {
				queueSize = queues()[id].size();
				print_debug("strdeque_size: deque " + std::to_string(id) + " size is: " + std::to_string(queueSize));
			}
			
			return queueSize;
		}
    
		void strdeque_insert_at(unsigned long id, size_t pos, const char* value) {
			if(id == constDequeId()) {
				const_strqueue("attemmpt to insert sth at const deque");
				return;
			}
            
			if(strdeque_exist(id) && value != NULL) {
				print_debug("strdeque_insert_at: (" + std::to_string(id) + ", " + std::to_string(pos) + ", val: "  +  std::string(value) + ")");
				if(pos >= queues()[id].size()) {
					queues()[id].push_back(value);
				} else {
					queues()[id].insert(queues()[id].begin() + pos, value);
				}
			}
            
			if(value == NULL) {
				print_debug("strdeque_insert_at: attempt to insert NULL into a deque: " + std::to_string(id));
			}
			
			if(!strdeque_exist(id)){
				print_debug("strdeque_insert_at: deque " + std::to_string(id) + "does not exist");
			}
		}
    
		void strdeque_remove_at(unsigned long id, size_t pos) {
			if(id == constDequeId()) {
				const_strqueue("attemmpt to remove sth from const deque");
				return;
			}
			if(strdeque_exist(id)) {
				if(pos < queues()[id].size()) {
					queues()[id].erase(queues()[id].begin() + pos);
					print_debug("strdeque_reomove_at: (id: " + std::to_string(id) + ", pos: " + std::to_string(pos) + ")");
				} else {
					print_debug("strdeque_remove_at: deque " + std::to_string(id) +
						"does not contain position:" + std::to_string(pos));
				}
			} else {
				print_debug("strdeque_remove_at: deque " + std::to_string(id) + "does not exist");
			}
		}
    
		const char* strdeque_get_at(unsigned long id, size_t pos) {
			if(id == constDequeId()) {
				const_strqueue("attemmpt to get sth from const deque");
				return NULL;
			}
            
			if(strdeque_exist(id)) {
				if(pos < queues()[id].size()) {
					const char* result = queues()[id][pos].c_str();
					print_debug("strdeque_get_at: (id: " + std::to_string(id) + ", pos: " + std::to_string(pos) + ") " + "val: " + std::string(result));
					return result;
				} else {
					print_debug("strdeque_get_at: deque " + std::to_string(id) +
						"does not contain position:" + std::to_string(pos));
					}
			}   else {
				print_debug("strdeque_get_at: deque " + std::to_string(id) + "does not exist");
				}
			return NULL;
		}
    
		void strdeque_clear(unsigned long id) {
			if(id == constDequeId()) {
				const_strqueue("attemmpt to clear const deque");
				return;
			}
            
			if(strdeque_exist(id)) {
				queues()[id].clear();
				print_debug("strdeque_clear: deque " + std::to_string(id));
			} else {
				print_debug("strdeque_clear: deque " + std::to_string(id) + " does not exist");
			}
		}
    
		int strdeque_comp(unsigned long id1, unsigned long id2) {
			int compResult = -2;
			if(strdeque_exist(id1) && strdeque_exist(id2)) {
				if(queues()[id1] > queues()[id2]) compResult = 1;
				else if(queues()[id1] < queues()[id2]) compResult = -1;
				else compResult = 0;
			} else if(strdeque_exist(id1)) {
				if(!strdeque_empty(id1)) compResult = 1;
				else compResult = 0;
			} else if(strdeque_exist(id2)) {
				if(!strdeque_empty(id2)) compResult = -1;
				else compResult = 0;
			} else compResult = 0;
			assert(compResult != -2);
			print_debug("Result of comparing deque " + std::to_string(id1) + " to deque " + std::to_string(id2) + " is: " + std::to_string(compResult));
			return compResult;
		}
	}
}
