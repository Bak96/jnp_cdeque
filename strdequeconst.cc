#include <iostream>
#include <string>

namespace {
	#ifndef NDEBUG
	const bool debug = true;
	#else
	const bool debug = false;
	#endif
	
	void print_debug(const std::string &message) {
		if (debug) {
			static std::ios_base::Init initializer;
			std::cerr << message << std::endl;
		}
	}
}

namespace jnp1 {
	extern "C" unsigned long emptystrdeque() {
		print_debug(std::string(__func__)+ "()");
		unsigned long idOfEmptyQueue = 0;
		return idOfEmptyQueue;
	}
}